#!/usr/bin/python
import string,sys,os.path, random
import ImageFile  # Thanks to http://wordaligned.org for the steganography code!
import pystega as pystega 

if len(sys.argv) < 2: 
  debug("DECODE BY: python steganography.py encryptedimage")
  debug("ENCODE BY: python steganography.py file picture picture2 ...")
  debug("Running with test arguments")
  sys.argv=[sys.argv[0], 'message.txt', 'picture.jpg']
messagepath = sys.argv[1]
picturepaths = sys.argv[2:]
numpictures = len(picturepaths)
def debug(*messages):
  print "Debug: ",
  for message in messages:
    print message, 
  print
def crash(*errors):
  print "Error: ",
  for error in errors:
    print error,
  print
  exit(1)


def getpic(picturepath):
  debug("Loading next picture...")
  
  try:
    picturepath = os.path.expandvars(os.path.expanduser(picturepath))
    picturepath = os.path.abspath(picturepath)
    if not os.path.exists(picturepath) or not os.path.isfile(picturepath):
      crash("Could not find picture", picturepath, " file ",picturepath)
    with open(picturepath, 'rb') as picturefile: 
      parser = ImageFile.Parser()
      nextdata = picturefile.read(1024)
      if not nextdata:
        crash( "no data")
      while nextdata:
        parser.feed(nextdata)
        nextdata = picturefile.read(1024)
      picture = parser.close()
      npixels = picture.size[0] * picture.size[1]
      nbands = len(picture.getbands())
      pichides = npixels*nbands/8
      debug("Picture ",picturepath, "loaded\n"+
      "       ",picturepath,"can hide", str(pichides), "bytes")
      return picture, picturepath , pichides     
  except Exception as e:
    crash("Could not open picture file ", picturepath,e) 


def encode(messagepath,picturepaths):
  basepid=random.randint(1000,8000)
  numpics = 0
  pid = 0
  debug("Encoding with picturepaths=", picturepaths)
  try:
    messagepath = os.path.expandvars(os.path.expanduser(messagepath))
    messagepath = os.path.abspath(messagepath)
    message = True
    if not os.path.exists(messagepath):
      crash("Could not find file ",messagepath)
    if os.path.isdir(messagepath):
      crash("TO BE IMPLEMENTED AFTER MY ESSAY")
    out =getpic(picturepaths[pid])
    pic, picturepath,plen = out
    with open(messagepath, 'r') as  messagefile:   
      message = messagefile.read()
    messagelen = len(message)
    processed = ""
    while message:
      pid = (pid+1)%numpictures
       
      numpics+=1
      pin = messagepath
      processing,message=message[:plen],message[plen:]
      processed+=processing
      hid=pystega.disguise(pic.copy(),processing, pin)
      hid2=hid.save("DCIM"+str(basepid+pid)+".png")
      debug("Picture #"+str(basepid+pid)+":", pic.mode, pic.format, "hid")
      debug(str(len(processing)) + "/" + str(plen) + " of file used")
      debug(str(len(processed))+ "/" + str(messagelen) + " of payload hidden")
    
  except Exception as e: 
    crash("Could not open message file ",str(e))

#Decode

if(len(sys.argv)==2):
  pid=sys.argv[1]
  while(pid[0] not in string.digits):
    pid=pid[1:]
  pid = int(os.path.splitext(pid)[0])
  picturepath="DCIM"+str(pid)+".png"
  while os.path.isfile(picturepath):
    out,pout=pystega.reveal(getpic(picturepath)[0])
    pid+=1
    picturepath="DCIM"+str(pid)+".png"
    print pout, out
else:
  encode(messagepath, picturepaths)


