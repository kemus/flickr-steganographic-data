flickr-stenographic-data
========================

Plan to Use stenography to turn 1 TB of flickr picture space into 1 TB of flickr HDD space. 

Thanks to Thomas Guest from http://wordaligned.org/ for his simple source code for steganography. Currently being used verbatim, will be modified to encode directory structure/etc. http://wordaligned.org/articles/steganography

Version 0.2:
Steganography! Hides the message in message.txt to the picture in picture.jpg, turning it into picture2.png (avoiding lossy compression)


